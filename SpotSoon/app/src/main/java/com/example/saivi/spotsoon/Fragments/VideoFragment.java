package com.example.saivi.spotsoon.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.saivi.spotsoon.Adapter.MoviesAdapter;
import com.example.saivi.spotsoon.DividerItemDecoration;
import com.example.saivi.spotsoon.Models.Movie;
import com.example.saivi.spotsoon.R;

import java.util.ArrayList;
import java.util.List;


public class VideoFragment extends Fragment {
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mAdapter = new MoviesAdapter(movieList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.video_fragment,container,false);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
//        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) v.findViewById(R.id.video_recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(mAdapter);
        prepareMovieData();
        return v;
    }

    private void prepareMovieData() {
        Movie movie = new Movie("Mad Max: Fury Road", "2 HOURS AGO", "In an apocalyptic world, a tyrant rules over a stark desert, controlling every drop of water.",R.drawable.avatar);
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "4 HOURS AGO", "With the IMF disbanded and the CIA hunting him.",R.drawable.crew);
        movieList.add(movie);

        movie = new Movie("Up", "6 HOURS AGO", "Carl, an old widower, goes off on an adventure in his flying hours.",R.drawable.fast_furious);
        movieList.add(movie);

        movie = new Movie("Star Trek", "8 HOURS AGO", "Star Trek is an American science fiction media franchise based on the television series.",R.drawable.golden_circle);
        movieList.add(movie);

        movie = new Movie("The LEGO Movie", "10 HOURS AGO", "Emmet, an ordinary individual prophesied to be the Special, is entrusted with a huge responsibility of saving the Lego world.",R.drawable.iron_man);
        movieList.add(movie);

        movie = new Movie("Iron Man", "12 HOURS AGO", "When an industrialist is captured, he constructs a high-tech armoured suit to escape. Once he manages to escape.",R.drawable.nemo);
        movieList.add(movie);

        movie = new Movie("Aliens", "14 HOURS AGO", "Ellen Ripley is sent back to the planet LV-426 to establish contact with a terraforming colony.",R.drawable.popeye);
        movieList.add(movie);

        movie = new Movie("Chicken Run", "16 HOURS AGO", "Rocky, a rooster, and Ginger, a chicken, decide to escape from a chicken farm.",R.drawable.six_bullets);
        movieList.add(movie);

        movie = new Movie("Back to the Future", "18 HOURS AGO", "Marty travels back in time using a time machine invented by an eccentric scientist. ",R.drawable.taking_earth);
        movieList.add(movie);

        movie = new Movie("Raiders of the Lost Ark", "20 HOURS AGO", "Star Trek is an American science fiction media franchise based on the television series.",R.drawable.thor);
        movieList.add(movie);

        movie = new Movie("Goldfinger", "22 HOURS AGO", "Carl, an old widower, goes off on an adventure in his flying house in search of Paradise Falls.",R.drawable.transofrmer);
        movieList.add(movie);

        movie = new Movie("Guardians of the Galaxy", "24 HOURS AGO", "In an apocalyptic world, a tyrant rules over a stark desert, controlling every drop of water.",R.drawable.avatar);
        movieList.add(movie);
        mAdapter.notifyDataSetChanged();
    }

}
