package com.example.saivi.spotsoon.Models;


public class Movie {
    private String title;
    private String time;

    public int getImage_resource() {
        return image_resource;
    }

    public void setImage_resource(int image_resource) {
        this.image_resource = image_resource;
    }

    private int image_resource;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String content;

    public Movie() {
    }

    public Movie(String title, String time, String content,int image_resource) {
        this.title = title;
        this.time = time;
        this.content = content;
        this.image_resource=image_resource;
    }


}
