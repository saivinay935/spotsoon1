package com.example.saivi.spotsoon.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.saivi.spotsoon.R;


/**
 * Created by saivi on 6/10/2017.
 */

public class ImageAdaper extends PagerAdapter {
    private int[] image_resources ={R.mipmap.chain_smoker,R.mipmap.michaeljack,R.mipmap.lorderoyal,R.mipmap.shape,R.mipmap.human};
    private String[] titles={"chain smoker new album 2016","michael jackson black or white","pop legend","shape of you","i am only human"};
    private String[] singers={"rihara and users","michael jackson","sakhira","ed seharan","human"};
    ImageView imageView;
    TextView tile,author;
    private Context ctx;
    private LayoutInflater layoutInflater;
    public ImageAdaper(Context ctx){
        this.ctx = ctx;
    }
    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view== object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.view_pager_layout,container,false);
        tile = (TextView) item_view.findViewById(R.id.title);
        author = (TextView) item_view.findViewById(R.id.sub_title);
        imageView = (ImageView) item_view.findViewById(R.id.img_pager_item);
        imageView.setImageResource(image_resources[position]);
        tile.setText(titles[position]);
        author.setText(singers[position]);
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
