package com.example.saivi.spotsoon.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.saivi.spotsoon.Models.Movie;
import com.example.saivi.spotsoon.R;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Movie> moviesList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, time, content;
        ImageView logo;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.movie_name);
            time = (TextView) view.findViewById(R.id.movie_time);
            content = (TextView) view.findViewById(R.id.movie_content);
            logo = (ImageView) view.findViewById(R.id.movie_icon);

        }
    }


    public MoviesAdapter(List<Movie> moviesList) {

        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movies_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Movie movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.time.setText(movie.getTime());
        holder.content.setText(movie.getContent());
        holder.logo.setImageResource(movie.getImage_resource());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}